#!/usr/bin/env python3
import sys


class Magasin(object):

    __list = []

    def __init__(self, lst=None):
        if lst is None:
            self.__list = []
        else:
            self.__list = lst

    def __len__(self):
        return len(self.__list)

    def tyhi(self):
        return len(self) == 0

    def push(self, value):
        self.__list.append(value)

    def pop(self):
        if self.tyhi():
            raise IndexError("pop tyhjast magasinist")
        return self.__list.pop()

    def tos(self):
        if self.tyhi():
            raise IndexError("tos tyhjast magasinist")
        return self.__list[len(self.__list)-1]

    def __bool__(self):
        return not self.tyhi()

    def __str__(self):
        res = ""
        for k in self.__list:
            res += " " + str(k)
        if res == "":
            res = "tyhi"
        return res

    def __eq__(self, other):
        if len(self) != len(other):
            return False
        for k in range(0, len(self)):
            if self.__list[k] != other.__list[k]:
                return False
        return True

    def copy(self):
        return Magasin(list(self.__list))

    def op(self, operator):
        if self.tyhi():
            raise IndexError("op tyhjast magasinist")
        o2 = self.pop()
        if self.tyhi():
            raise IndexError("op pooltyhjast magasinist")
        o1 = self.pop()
        if operator.strip() in ['+', '-', '*', '/']:
            res = eval(str(o1) + operator + str(o2))
        else:
            raise ValueError("vale tehe " + operator + " meetodis op")
        self.push(res)

    @staticmethod
    def interpret(avaldis):
        if len(avaldis.strip()) == 0:
            raise ValueError("Tyhi avaldis")
# TODO!!!


def main():
    expr = ""
    print(expr)


if __name__ == '__main__':
    main()

