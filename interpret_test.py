#!/usr/bin/env python3
import magasin
import unittest


class MyTestCase(unittest.TestCase):

    def test_interpret(self):
        res = magasin.Magasin.interpret("-234")
        self.assertEqual(-234, res, "avaldise -234 tulemus peaks olema -234")
        res = magasin.Magasin.interpret("-234 234 + 3 /")
        self.assertEqual(0, res, "avaldise -234 234 + 3 / tulemus peaks olema 0")
        res = magasin.Magasin.interpret("1 2 3 4 5 + - * +")
        self.assertEqual(-11, res, "avaldise 1 2 3 4 5 + - * + tulemus peaks olema -11")

    def test_parse_empty1(self):
        with self.assertRaises(Exception, msg="tyhi avaldis peaks olema viga"):
            magasin.Magasin.interpret("")

    def test_parse_empty2(self):
        with self.assertRaises(Exception, msg="Valgetest tyhikutest koosnev avaldis peaks olema viga"):
            magasin.Magasin.interpret("             ")
    
    def test_parse_invalidsymbol1(self):
        with self.assertRaises(Exception, msg="tundmatu symbol x peaks olema viga"):
            magasin.Magasin.interpret("2 x")

    def test_parse_invalidsymbol2(self):
        with self.assertRaises(Exception, msg="tundmatu symbol x peaks olema viga"):
            magasin.Magasin.interpret("2 5 - x")

    def test_parse_invalidsymbol3(self):
        with self.assertRaises(Exception, msg="tundmatu symbol ? peaks olema viga"):
            magasin.Magasin.interpret("2 5 - ?")

    def test_parse_underflow1(self):
        with self.assertRaises(Exception, msg="avaldis 2 - peaks olema viga"):
            magasin.Magasin.interpret("2 -")

    def test_parse_underflow2(self):
        with self.assertRaises(Exception, msg="avaldis 1 3 + - 5 peaks olema viga"):
            magasin.Magasin.interpret("1 3 + - 5")

    def test_toomany1(self):
        with self.assertRaises(Exception, msg="avaldis 2 3 peaks olema viga"):
            magasin.Magasin.interpret("2 3")

    def test_toomany2(self):
        with self.assertRaises(Exception, msg="avaldis 2 3 + 4 8 - peaks olema viga"):
            magasin.Magasin.interpret("2 3 + 4 8 -")


if __name__ == '__main__':
    unittest.main()
